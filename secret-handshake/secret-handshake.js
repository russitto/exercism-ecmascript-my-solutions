const comms = [
  {code: 1, message: 'wink'},
  {code: 2, message: 'double blink'},
  {code: 4, message: 'close your eyes'},
  {code: 8, message: 'jump'}
]

class SecretHandshake {
  constructor(num) {
    if (typeof num != 'number') {
      throw new Error('Handshake must be a number')
    }
    this.num = num
  }

  commands() {
    let out = []
    comms.forEach(comm => {
      if ((this.num & comm.code) == comm.code) {
        out.push(comm.message)
      }
    })
    if ((this.num & 16) == 16) {
      out = out.reverse()
    }
    return out
  }
}

export default SecretHandshake
