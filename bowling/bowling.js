const GAME_NOT_ENDED = 'Score cannot be taken until the end of the game'
const GAME_ALREADY_ENDED = 'Should not be able to roll after game is over'
const VALUE_OUT_RANGE = 'Pins must have a value from 0 to 10'
const PIN_EXCEEDED = 'Pin count exceeds pins on the lane'

class Bowling {
  constructor(rolls) {
    this.rolls = rolls
  }

  score() {
    let sum = 0
    let frames = 0
    let i
    let bonus
    let donus

    for (i = 0; i < this.rolls.length && frames < 10; i++) {
      let it0 = this.rolls[i]
      if (it0 < 0 || it0 > 10) throw new Error(VALUE_OUT_RANGE)
      sum += it0
      bonus = donus = false

      if (it0 == 10) {
        let it0 = this.rolls[i+1]
        let it1 = this.rolls[i+2]
        if (typeof it0 == 'undefined' || typeof it1 == 'undefined') {
          throw new Error(GAME_NOT_ENDED)
        }
        if (it1 < 0 || it1 > 10) throw new Error(VALUE_OUT_RANGE)
        if ((it0 || it1) && it0 != 10 && it0 + it1 > 10) throw new Error(PIN_EXCEEDED)
        sum += it0 + it1
        donus = true
      } else {
        let it1 = this.rolls[++i]
        if (typeof it1 == 'undefined') throw new Error(GAME_NOT_ENDED)
        if (it1 < 0 || it1 > 10) throw new Error(VALUE_OUT_RANGE)
        if (it0 + it1 > 10) throw new Error(PIN_EXCEEDED)
        sum += it1
        if (it0 + it1 == 10) {
          let it1 = this.rolls[i + 1]
          if (typeof it1 == 'undefined') throw new Error(GAME_NOT_ENDED)
          sum += it1
          bonus = true
        }
      }
      frames++
    }
    if (frames < 10) throw new Error(GAME_NOT_ENDED)
    if (frames > 10) throw new Error(GAME_ALREADY_ENDED)
    if (frames == 10 && i < this.rolls.length && !bonus && !donus) throw new Error(GAME_ALREADY_ENDED)
    return sum
  }
}

export default Bowling
