class Complex {
  constructor(real, imag) {
    this.real = real
    this.imag = imag
    this.abs = Math.sqrt(Math.pow(real, 2) + Math.pow(imag, 2))
  }

  add(compl) {
    return new Complex(this.real + compl.real, this.imag + compl.imag)
  }

  neg() {
    return new Complex(-this.real, -this.imag)
  }

  sub(compl) {
    return this.add(compl.neg())
  }

  mul(compl) {
    return new Complex(this.real * compl.real - this.imag * compl.imag
      , this.imag * compl.real + this.real * compl.imag)
  }

  div(compl) {
    let out = new Complex(0, 0)
    let pows = Math.pow(compl.real, 2) + Math.pow(compl.imag, 2)
    let real = (this.real * compl.real + this.imag * compl.imag) / pows
    let imag = (this.imag * compl.real - this.real * compl.imag) / pows
    return new Complex(real, imag)
  }

  get conj() {
    return new Complex(this.real, this.imag == 0? 0: -this.imag)
  }

  get exp() {
    let first = new Complex(Math.exp(this.real), 0)
    let second = new Complex(Math.cos(this.imag), Math.sin(this.imag))
    return first.mul(second)
  }
}

export default Complex
