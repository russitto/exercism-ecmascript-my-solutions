export default bracket

function bracket(str) {
  let last
  do {
    last = str
    str = str.replace(/(\[\]|\(\)|\{\})/, '')
  } while (str != last && str != '')
  return str == ''
}
