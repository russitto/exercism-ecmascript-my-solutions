class Octal {
  constructor(number) {
    const filtered = number.replace(/[^0-7]/g, '')
    this.decimal = 0
    if (number == filtered) {
      let pow = 0
      for (let i = number.length - 1; i >= 0; i--) {
        this.decimal += 8 ** (number.length - 1 - i) * number[i]
      }
    }
  }

  toDecimal() {
    return this.decimal
  }
}

export default Octal
