class School {
  constructor() {
    this.db = {}
  }

  add(name, grade) {
    if (!this.db[grade]) {
      this.db[grade] =[]
    }
    this.db[grade].push(name)
    this.db[grade] = this.db[grade].sort()
  }

  grade(gr) {
    if (!this.db[gr]) return []
    return Object.assign([], this.db[gr])
  }

  roster() {
    let out = {}
    for (let grade in this.db) {
      out[grade] = this.grade(grade)
    }
    return out
  }
}

export default School
//let sc = new School()
//sc.add('Matias', 4)
//console.log(sc.roster())
