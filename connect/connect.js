class Path {
  constructor(){
    this.idx = []
    this.elements = []
  }

  get length() {
    return this.idx.length
  }

  push(el) {
    if (this.elements.length == 0 && ['X', 'O'].indexOf(el.l) >= 0
      || !this.passed(el.idx) && el.l == this.elements[0].l) {
      this.idx.push(el.idx)
      this.elements.push(el)
      return true
    }
    return false
  }

  passed(idx) {
    return this.idx.indexOf(idx) >= 0
  }

  static winner(values) {
    values = values.map(el => el.replace(/ /g, '').split(''))
    let sizeI = values.length
    let sizeJ = sizeI? values[0].length: 0
    let pathX = new Path()
    pathX.fillX(values)
    if (pathX.complete(sizeI, sizeJ)) return 'X'
    let pathO = new Path()
    pathO.fillO(values)
    if (pathO.complete(sizeI, sizeJ)) return 'O'
    return ''
  }

  complete(sizeI, sizeJ) {
    if (!this.elements.length || sizeI == 0 || sizeJ == 0) return false
    let l = this.elements[0].l
    let west = /^[0-9]+-0$/
    let east = new RegExp('^[0-9]+-' + (sizeJ-1) + '$')
    let north = /^0-[0-9]+$/
    let south = new RegExp('^' + (sizeI-1) + '-[0-9]$')
    if (l == 'X') {
      return find(this.idx, west) && find(this.idx, east)
    }
    if (l == 'O') {
      return find(this.idx, north) && find(this.idx, south)
    }
  }

  fillX(values) {
    for (let i = 0; i < values.length; i++) {
      if (values[i][0] == 'X') {
        this.push({
          l: values[i][0],
          i,
          j: 0,
          idx: i+'-0'
        })
      }
    }
    this._fill(values)
  }

  fillO(values) {
    if (!values.length) return
    for (let j = 0; j < values[0].length; j++) {
      if (values[0][j] == 'O') {
        this.push({
          l: values[0][j],
          i: 0,
          j,
          idx: '0-' + j
        })
      }
    }
    this._fill(values)
  }

  _fill(values) {
    let moves
    if (this.length) {
      let moves = this.elements
      do {
        let m = []
        moves.forEach(el => {
          let possibles = move(values, el.i, el.j)
            .filter(e => this.push({
              l: values[e[0]][e[1]],
              i: e[0],
              j: e[1],
              idx: e[0] + '-' + e[1]
            }))
          m = [...m, ...possibles.map(p => {
            return {
              l: values[p[0]][p[1]],
              i: p[0],
              j: p[1],
              idx: p[0] + '-' + p[1]
            }
          })]
        })
        moves = m
      } while (moves.length)
    }
  }
}

class Connect {
  constructor(values) {
    this.values = values
  }

  winner() {
    return Path.winner(this.values)
  }
}

export default Connect

function move(values, i, j) {
  // horizontal + vertical + NE-SW
  let out = []
  if (def(values[i-1])) {
    out.push([i-1, j])
    if (def(values[i-1][j+1])) out.push([i-1, j+1])
  }
  if (def(values[i+1])) {
    out.push([i+1, j])
    if (def(values[i+1][j-1])) out.push([i+1, j-1])
  }
  if (def(values[i][j-1])) out.push([i, j-1])
  if (def(values[i][j+1])) out.push([i, j+1])
  return out
}

function def(el) {
  return typeof el != 'undefined'
}

function find(arr, reg) {
  let out = false
  for (let i = 0; !out && i < arr.length; i++) {
    out = reg.test(arr[i])
  }
  return out
}
