const noMore = `No more bottles of beer on the wall, no more bottles of beer.
Go to the store and buy some more, 99 bottles of beer on the wall.
`

class Beer {
  static verse(num) {
    if (num == 0) return noMore

    let formatted = fBottle(num)
    let take = 'Take one'
    if (num == 1) take = 'Take it'

    return `${formatted} of beer on the wall, ${formatted} of beer.
${take} down and pass it around, ${fBottle(num-1)} of beer on the wall.
`
  }

  static sing(from = 99, to = 0) {
    let out = []
    for (let i = from; i >= to; i--) {
      out.push(this.verse(i))
    }
    return out.join('\n')
  }
}

function fBottle(num) {
  if (num < 1) {
    return `no more bottles`
  }
  if (num == 1) {
    return `${num} bottle`
  }
  return `${num} bottles`
}

export default Beer
//let b = new Beer()
//console.log(b.verse(0))
//console.log(b.verse(2))
