class PascalsTriangle {
  constructor(numRow) {
    this.rows = genTriangle(numRow)
    this.lastRow = this.rows[this.rows.length-1]
  }
}

function genTriangle(numRow) {
  if (numRow < 0) return [[]]
  let out = [[1]]
  for (let i = 1; i < numRow; i++) {
    out[i] = []
    for (let j = 0; j <= i; j++) {
      out[i][j] = (out[i-1][j-1] || 0) + (out[i-1][j] || 0)
    }
  }
  return out
}

export default PascalsTriangle
