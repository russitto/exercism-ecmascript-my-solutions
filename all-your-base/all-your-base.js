const WRONG_FORMAT = new Error('Input has wrong format')
const WRONG_INP_BASE = new Error('Wrong input base')
const WRONG_OUT_BASE = new Error('Wrong output base')

class Bases {
  convert(digits, from, to) {
    if (isNaN(from)) throw WRONG_INP_BASE
    if (parseInt(from) != from) throw WRONG_INP_BASE
    if (isNaN(to)) throw WRONG_OUT_BASE
    if (parseInt(to) != to) throw WRONG_OUT_BASE
    if (from < 2) throw WRONG_INP_BASE
    if (to < 2) throw WRONG_OUT_BASE
    if (digits.length == 0) throw WRONG_FORMAT
    if (digits.length > 1 && digits[0] == 0) throw WRONG_FORMAT
    if (Math.min(...digits) < 0) throw WRONG_FORMAT
    if (Math.max(...digits) >= from) throw WRONG_FORMAT
    return from10(to10(digits, from), to)
  }
}

export default Bases

function to10(digits, from) {
  digits.reverse()
  let mult = 1
  let sum = 0

  for (let i = 0; i < digits.length; i++) {
    sum += digits[i] * mult
    mult *= from
  }
  return sum
}

function from10(num, to) {
  let mult = to
  let out = []
  let pows = []

  let pow = 1
  while (pow <= num) {
    pows.push(pow)
    pow *= to
  }
  pows.reverse()

  for (let i = 0; i < pows.length; i++) {
    let digit = parseInt(num / pows[i])
    out.push(digit)
    num -= digit * pows[i]
  }
  if (!out.length) out = [0]
  return out
}
