const size = 8
const defPos = {
  white: [0, size/2-1],
  black: [size - 1, size/2-1]
}

const sameSpaceError = 'Queens cannot share the same space'

class Queens {
  constructor(position = defPos) {
    if (eqArr(position.white, position.black)) throw sameSpaceError
    this.white = position.white
    this.black = position.black
    this.string = buildString(position)
  }

  toString() {
    return this.string
  }

  canAttack() {
    if (this.white[0] == this.black[0] ||
      this.white[1] == this.black[1]) {
      return true
    }
    const diffI = this.white[0] - this.black[0]
    const diffJ = this.white[1] - this.black[1]
    if (Math.abs(diffI) == Math.abs(diffJ)) {
      return true
    }
    return false
  }
}

export default Queens

function eqArr(arr1, arr2) {
  if (arr1.length != arr2.length) return false
  let out = true
  for (let i = 0; out && i < arr1.length; i++) {
    out = arr1[i] == arr2[i]
  }
  return out
}

function buildString(position) {
  let arr = new Array(size).fill(undefined).map(el => new Array(size).fill('_'))
  let i = position.white[0]
  let j = position.white[1]
  arr[i][j] = 'W'
  i = position.black[0]
  j = position.black[1]
  arr[i][j] = 'B'
  return arr.map(el => el.join(' ')).join('\n') + '\n'
}
