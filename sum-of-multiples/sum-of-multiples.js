export default SumOfMultiples

function SumOfMultiples(arr) {
  const numbers = arr.filter((el, i) => arr.indexOf(el) ==  i).sort((a, b) => a-b)

  return { to }

  function to(num) {
    let out = 0
    let added = -1
    for (let i = numbers[0]; i < num; i++) {
      for (let j = 0; j < numbers.length; j++) {
        if (added != i && !(i % numbers[j])) {
          out += i
          added = i
        }
      }
    }
    return out
  }
}
