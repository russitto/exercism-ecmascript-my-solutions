export default class Transcription {
  toRna(strand) {
    let out = ''
    for (let i = 0; i < strand.length; i++) {
      out += charToRna(strand[i])
    }
    if (strand.length != out.length) {
      throw new Error('Invalid input DNA.')
    }
    return out
  }
}

function charToRna(char) {
  switch(char) {
    case 'C':
      char = 'G'
      break
    case 'G':
      char = 'C'
      break
    case 'T':
      char = 'A'
      break
    case 'A':
      char = 'U'
      break
    default:
      char =''
  }
  return char
}
