function ArgumentError(message) {
  this.name = 'ArgumentError'
  //this.message =  message || 'Default Message'
  //this.stack = (new Error()).stack
}

ArgumentError.prototype = Object.create(Error.prototype)
ArgumentError.prototype.constructor = ArgumentError

class WordProblem {
  constructor(question) {
    this.question = question
  }

  answer() {
    let question = this.question.trim()
    if (!question.trim().match(/^What is .*\?$/)) {
      throw new ArgumentError()
    }
    question = question.replace(/^What is\s+/, '').replace(/\s*\?$/, '')
    this.count = parse(question)
    return this.count
  }
}

export { WordProblem, ArgumentError }

function parse(msg) {
  let words = []
  let count = 0
  let spl = msg.split(' ')

  for (let i = 0; i < spl.length; i++) {
    let int = parseInt(spl[i])
    if (int == spl[i]) {
      words.push(int)
    } else if (spl[i] == 'plus') {
      words.push('+')
    } else if (spl[i] == 'minus') {
      words.push('-')
    } else if (spl[i] == 'divided' && i < spl.length -1 && spl[i+1] == 'by') {
      words.push('/')
      i++
    } else if (spl[i] == 'multiplied' && i < spl.length -1 && spl[i+1] == 'by') {
      words.push('*')
      i++
    } else {
      throw new ArgumentError()
    }
  }
  if (words.length == 0) {
    throw new ArgumentError()
  }

  for (let i = 0; i < words.length; i++) {
    if (i % 2 && typeof words[i] == 'number') {
      throw new ArgumentError()
    }
    if (!(i % 2) && typeof words[i] != 'number') {
      throw new ArgumentError()
    }
  }

  count = words[0]
  for (let i = 1; i < words.length-1; i++) {
    if (words[i] == '+') {
      count += words[i+1]
      i++
    }
    if (words[i] == '-') {
      count -= words[i+1]
      i++
    }
    if (words[i] == '*') {
      count *= words[i+1]
      i++
    }
    if (words[i] == '/') {
      console.log(count, words[i-1], words[i], words[i+1])
      count /= words[i+1]
      i++
      console.log('partiaaaal', count)
    }
  }

  return count
}
