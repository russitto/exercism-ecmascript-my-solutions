const giga = 1000000000000

class Gigasecond {
  constructor(d) {
    const plusGiga = d.getTime() + giga
    this.datt = new Date(plusGiga)
  }

  date() {
    return this.datt
  }
}

export default Gigasecond
