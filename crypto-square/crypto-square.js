class CryptoSquare {
  constructor(message) {
    this.message = message.toLowerCase().replace(/[^a-z0-9]/g, '')

    const square = Math.sqrt(this.message.length)
    this.r = Math.floor(square)
    this.c = Math.ceil(square)

    this.reg = new RegExp('.{1,' + this.c + '}', 'g')
    this.segments = this.message.match(this.reg)
  }

  normalizePlaintext() {
    return this.message
  }

  size() {
    return this.c
  }

  plaintextSegments() {
    return this.segments
  }

  ciphertext() {
    let ciphered = ''
    if (!this.segments) {
      return ''
    }
    for (let j = 0; j < this.segments[0].length; j++) {
      for (let i = 0; i < this.segments.length; i++) {
        ciphered += this.segments[i][j] || ''
      }
    }
    return ciphered
  }
}

export default CryptoSquare
