class Series {
  constructor(number) {
    this.digits = number.split('').map(s => parseInt(s))
  }

  slices(num) {
    if (num > this.digits.length) {
      throw new Error('Slice size is too big.')
    }
    if (num <= 1) {
      return this.digits.map(el => [el])
    }
    let out = []
    for (let i = 0; i < this.digits.length - num + 1; i++) {
      out.push(this.digits.slice(i, i+num))
    }
    return out
  }
}

export default Series
