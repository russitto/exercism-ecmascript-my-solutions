function BufferFullError(message) {
  this.name = 'BufferFullError'
}
BufferFullError.prototype = Object.create(Error.prototype)
function BufferEmptyError(message) {
  this.name = 'BufferEmptyError'
}
BufferEmptyError.prototype = Object.create(Error.prototype)

// export BufferFullError, BufferEmptyError
export default buffer

function buffer(size) {
  let points = [0,0]
  let buf = Array(size)
  let firstWrite = true
  let lastAction = ''

  function read() {
    if (points[0] >= points[1] || firstWrite) {
      if (lastAction != 'w') throw new BufferEmptyError()
    }
    points[0] %= size
    let out = buf[points[0]]
    points[0]++
    if ((points[0]%size) == (points[1]%size)) {
      firstWrite = true
    }
    lastAction = 'r'
    return out
  }

  function write(el) {
    if (typeof el == 'undefined' || el === null) return
    if ((points[0]%size) == (points[1]%size) && !firstWrite) {
      throw new BufferFullError()
    }
    points[1] %= size
    buf[points[1]++] = el
    firstWrite = false
    lastAction = 'w'
  }

  function forceWrite(el) {
    try {
      write(el)
    } catch (e) {
      points[0]++
      buf[points[1]%size] = el
      points[1]++
      firstWrite = false
      lastAction = 'w'
    }
  }

  function clear() {
    buf = Array(size)
    points = [0,0]
    firstWrite = true
    lastAction = ''
  }

  return {
    BufferFullError,
    BufferEmptyError,
    read,
    write,
    forceWrite,
    clear
  }
}
