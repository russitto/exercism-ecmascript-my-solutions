class Pyth {
  constructor(a, b, c) {
    this.sides = [a, b, c].sort((a, b) => a - b)
    this.squares = this.sides.map(s => s**2)
  }

  sum() {
    return this.sides.reduce((accum, val) => accum + val)
  }

  product() {
    return this.sides.reduce((accum, val) => accum * val)
  }

  isPythagorean() {
    return this.squares[0] + this.squares[1] == this.squares[2]
  }

  static where(condition) {
    const min = condition.minFactor || 1
    const max = condition.maxFactor
    const sum = condition.sum || 0

    const out = []
    for (let i = min; i < max; i++) {
      for (let j = i; j < max; j++) {
        for (let k = j; k <= max; k++) {
          if (!sum || i + j + k == sum) {
            const p = new Pyth(i, j, k)
            if (p.isPythagorean()) {
              out.push(p)
            }
          }
        }
      }
    }
    return out
  }
}

export default Pyth
