class Cipher {
  constructor(key = false) {
    this.key = this.invKey = []
    const charA = 'a'.charCodeAt(0)
    const charZ = 'z'.charCodeAt(0)
    if (!key) {
      key = ''
      for (let i = 0; i < Math.random() * 20 + 10; i++) {
        let k = Math.floor(Math.random() * (charZ - charA))
        this.key.push(k)
        this.invKey.push(-k)
      }
    } else if (!key.match(/^[a-z]+$/)) {
      throw new Error('Bad key')
    } else {
      for (let i = 0; i < key.length; i++) {
        let k = key.charCodeAt(i) - charA
        this.key.push(k)
        this.invKey.push(-k)
      }
    }
  }

  encode(str) {
    return encodeAux(str, this.key)
  }

  decode(str) {
    return encodeAux(str, this.invKey)
  }
}

function encodeAux(str, key) {
  const charA = 'a'.charCodeAt(0)
  const charZ = 'z'.charCodeAt(0)
  let out = ''
  for (let i = 0; i < str.length; i++) {
    let inc = key[i] || 0
    let outChar = str.charCodeAt(i) + inc
    if (outChar > charZ) {
      // out of range a-z
      outChar -= charA
    }
    out += String.fromCharCode(outChar)
  }
  return out
}

let c = new Cipher('j')
console.log(c.decode('j'))
