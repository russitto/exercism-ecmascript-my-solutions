export default {rotate}
const LOWER = 'abcdefghijklmnopqrstuvwxyz'
const UPPER = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
const SIZE = 26

function rotate(str, num) {
  let out = ''
  for (let i = 0; i < str.length; i++) {
    let idxl = LOWER.indexOf(str[i])
    let idxu = UPPER.indexOf(str[i])
    if (idxl >= 0) {
      idxl = (idxl + num) % SIZE
      out += LOWER[idxl]
    } else if (idxu >= 0) {
      idxu = (idxu + num) % SIZE
      out += UPPER[idxu]
    } else {
      out += str[i]
    }
  }
  return out
}
