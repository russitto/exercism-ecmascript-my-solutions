export default generate

function generate(options = {}) {
  const min = options.minFactor || 1
  const max = options.maxFactor || 9

  let palindromes = []

  for (let i = min; i <= max; i++) {
    for (let j = i; j <= max; j++) {
      let value = i * j
      if (isPalindromic(value)) {
        palindromes.push({
          factors: [i, j],
          value
        })
      }
    }
  }

  const smallest = palindromes.reduce((actual, current) => current.value < actual.value? current: actual)
  const largest  = palindromes.reduce((actual, current) => current.value > actual.value? current: actual)

  return { smallest, largest }

  function isPalindromic(num) {
    const left = num.toString()
    const right = left.split('').reverse().join('')
    return left == right
  }
}
