export default meetupDay

const WEEK_DAYS = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday'
]

function meetupDay(year, month, weekDay, position) {
  const firstOfMonth = new Date(year, month, 0)
  const weekDayPos = WEEK_DAYS.indexOf(weekDay)
  let weekDiff = weekDayPos - firstOfMonth.getDay()
  if (weekDiff < 0) weekDiff += 7

  if (position == 'teenth') {
    let plus = weekDiff + 7
    if (plus > 12 && plus < 20) {
      return new Date(year, month, plus)
    }
    return new Date(year, month, plus + 7)
  }
  let lastDay = getDaysOfMonth(year, month)
  if (position == 'last') {
    let plus = weekDiff + 28
    if (plus <= lastDay) {
      return new Date(year, month, plus)
    }
    return new Date(year, month, plus - 7)
  }

  if (weekDiff == 0) weekDiff = 7
  let weeks = parseInt(position[0]) - 1
  let plus = weekDiff + weeks * 7
  if (plus > lastDay) throw 'Invalid date'
  return new Date(year, month, plus)
}

function getDaysOfMonth(year, month) {
  return new Date(year, month + 1, 0).getDate()
}
