class Acronyms {
  static parse(str) {
    str = str.replace(/\,\.\:/g, '')
    str = str.replace(/([a-z])([A-Z])/, '$1 $2')
    const words = str.split(/(\s|-)/).filter(w => w.trim() != '' && w.trim() != '-')
    return words.map(w => w.substr(0, 1)).join('').toUpperCase()
  }
}

export default Acronyms
