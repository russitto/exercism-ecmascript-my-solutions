class Hexadecimal {
  constructor(number) {
    const filtered = number.replace(/[^0-9a-f]/g, '')
    this.decimal = 0
    if (number == filtered) {
      let pow = 0
      for (let i = number.length - 1; i >= 0; i--) {
        let num = number[i]
        if (isNaN(num)) {
          num = num.charCodeAt(0) - 87 // 97 = a, -87 => 'a' = 10, 'b' = 11
        }
        this.decimal += 16 ** (number.length - 1 - i) * num
      }
    }
  }

  toDecimal() {
    return this.decimal
  }
}

export default Hexadecimal
