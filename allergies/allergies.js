const all = ['eggs', 'peanuts', 'shellfish', 'strawberries', 'tomatoes', 'chocolate', 'pollen', 'cats']

class Allergies {
  constructor(num) {
    let ord = 0
    this.lis = all.filter(al => (1<<ord++) & num)
  }

  list() {
    return this.lis
  }

  allergicTo(str) {
    return this.lis.indexOf(str) >= 0
  }
}

export default Allergies
