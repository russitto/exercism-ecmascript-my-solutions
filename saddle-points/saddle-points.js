class Matrix {
  constructor(init) {
    this.rows = init.split('\n').map(el => el.trim().split(' '))
    if (this.rows.length) {
      const sizeI = this.rows.length
      const sizeJ = this.rows[0].length
      this.columns = new Array(sizeJ).fill(undefined).map(el => new Array(sizeI))
      for (let i = 0; i < this.rows.length; i++) {
        for (let j = 0; j < this.rows[0].length; j++) {
          this.columns[j][i] = this.rows[i][j] = parseInt(this.rows[i][j])
        }
      }
    }
  }

  get saddlePoints () {
    let out = []
    for (let i = 0; i < this.rows.length; i++) {
      for (let j = 0; j < this.rows[0].length; j++) {
        let el = this.rows[i][j]
        if (isMax(el, this.rows[i]) && isMin(el, this.columns[j])) {
          out.push([i, j])
        }
      }
    }
    return out
  }
}

export default Matrix

function isMin(el, arr) {
  let out = true
  for (let i = 0; out && i < arr.length; i++) {
    out = el <= arr[i]
  }
  return out
}

function isMax(el, arr) {
  let out = true
  for (let i = 0; out && i < arr.length; i++) {
    out = el >= arr[i]
  }
  return out
}
