const ySep = '\n'
const xSep = ' '

class Matrix {
  constructor(sMatr) {
    this.rows = sMatr.split(ySep)
      .map(row => row.split(xSep)
        .map(n => parseInt(n)))

    this.columns = []
    for (let i = 0; i < this.rows.length; i++) {
      for (let j = 0; j < this.rows[i].length; j++) {
        if (typeof this.columns[j] == 'undefined') {
          this.columns[j] = []
        }
        this.columns[j][i] = this.rows[i][j]
      }
    }
  }
}

export default Matrix
