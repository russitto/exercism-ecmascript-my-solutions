export default class Year {
  constructor(year) {
    this.year = year
  }

  isLeap() {
    if (!(this.year % 400)) return true
    if (!(this.year % 100)) return false
    if (!(this.year % 4)) return true
    return false
  }
}
