class Clock {
  constructor(hours, minutes = 0) {
    this.hours = mod(hours + Math.floor(minutes / 60), 24)
    this.minutes = mod(minutes, 60)
  }

  toString() {
    return zeroFill(this.hours) + ':' + zeroFill(this.minutes)
  }

  plus(minutes = 0) {
    return new Clock(this.hours, this.minutes + minutes)
  }

  minus(minutes = 0) {
    return this.plus(minutes*-1)
  }

  equals(clo) {
    return this.hours == clo.hours && this.minutes == clo.minutes
  }
}

export default at

function at(hours, minutes = 0) {
  return new Clock(hours, minutes)
}


function zeroFill(n) {
  return n.toLocaleString('en', {minimumIntegerDigits: 2})
}

function mod(num, mod) {
  let ret = num % mod
  if (ret >= 0) {
    return ret
  }
  return ret + mod
}
