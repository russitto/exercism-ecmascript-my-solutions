class Flattener {
  flatten(elements) {
    let out = []
    elements.forEach(el => {
      if (['number', 'string'].indexOf(typeof el) >= 0) {
        out.push(el)
      } else {
        if (typeof el != 'undefined' && el != null) {
        this.flatten(el).forEach(e => out.push(e))
        }
      }
    })
    return out
  }
}

export default Flattener
