class BinarySearch {
  constructor(arr) {
    if (!isSorted(arr)) return
    this.array = arr
  }

  indexOf(num) {
    let found = false
    let left = 0
    let right = this.array.length
    let idx = 0
    while (!found && idx != -1) {
      idx = left + right >> 1
      if (this.array[idx] == num) {
        found = true
      } else {
        if (idx == left || idx >= right - 1) {
          found = false
          idx = -1
        } else if (num < this.array[idx]) {
          right = idx
        } else  {
          left = idx
        }
      }
    }
    if (!found) return -1
    return idx
  }
}

export default BinarySearch

function isSorted(arr) {
  let out = true
  for (let i = 0; out && i < arr.length - 1; i++) {
    for (let j = i + 1; out && j < arr.length; j++) {
      out = arr[i] <= arr[j]
    }
  }
  return out
}
