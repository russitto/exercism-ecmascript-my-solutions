class Words {
  count(phrase) {
    const clean = phrase.trim().toLowerCase().replace(/(\s|\n)+/g, ' ')
    const words = clean.split(' ')

    const ret = {}
    words.forEach(w => {
      if (typeof ret[w] != 'number') {
        ret[w] = 0
      }
      ret[w]++
    })
    return ret
  }
}

export default Words
