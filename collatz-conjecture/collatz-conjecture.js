export default function steps(num) {
  if (num <= 0) {
    throw new Error('Only positive numbers are allowed')
  }
  let ret = 0
  while (num != 1) {
    if (num % 2 == 0) {
      num /= 2
    } else {
      num *= 3
      num++
    }
    ret++
  }
  return ret
}
