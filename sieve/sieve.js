class Sieve {
  constructor(max) {
    this.primes = [...Array(max-1).keys()].map(el => el + 2)
    for (let i = 0; i < this.primes.length; i++) {
      this.primes = this.primes.filter(p => {
        const el = this.primes[i]
        return p == el || p % el
      })
    }
  }
}

export default Sieve
