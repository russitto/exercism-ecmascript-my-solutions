const table = {
  'M' : 1000,
  'CM': 900,
  'D' : 500,
  'CD': 400,
  'C' : 100,
  'XC': 90,
  'L' : 50,
  'XL': 40,
  'X' : 10,
  'IX': 9,
  'V' : 5,
  'IV': 4,
}

export default toRoman
function toRoman(num) {
  let out = ''
  for (let l in table) {
    const times = parseInt(num / table[l])
    out += repeat(l, times)
    num %= table[l]
  }
  return out + repeat('I', num)
}

function repeat(l, times) {
  return Array(times + 1).join(l)
}
