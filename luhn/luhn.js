class Luhn {
  constructor(message) {
    this.valid = false
    this.number = message.replace(/[^0-9 ]/g, '')
    if (message == this.number) {
      this.number = this.number.replace(/ /g, '')
      if (this.number.length > 1) {
        const reverse = this.number.split('').reverse().map(s => parseInt(s))
        let total = 0
        for (let i = 0; i < reverse.length; i++) {
          if (i % 2) {
            total += reverse[i] * 2 % 9
          } else {
            total += reverse[i]
          }
        }
        this.valid = (total % 10) == 0
      }
    }
  }
}

export default Luhn
