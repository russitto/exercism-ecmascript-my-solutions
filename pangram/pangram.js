const codeA = 'a'.charCodeAt(0)
const codeZ = 'z'.charCodeAt(0)

class Pangram {
  constructor(str) {
    this.str = str.toLowerCase()
  }

  isPangram() {
    if (this.str.length < codeZ+1 - codeA) {
      return false
    }

    let out = true
    for (let i = codeA; out && i <= codeZ; i++) {
      out = this.str.indexOf(String.fromCharCode(i)) >= 0
    }
    return out
  }
}

export default Pangram
