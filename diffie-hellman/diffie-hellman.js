class Diffie {
  constructor(p, g) {
    if (p < 2 || g < 2 || p > 9000 || g > 9000) throw 'Out of range'
    if (!isPrime(p) || !isPrime(g)) throw 'Use prime numbers'
    this.p = p
    this.g = g
  }

  getPublicKeyFromPrivateKey(a) {
    if (a < 2 || a >= this.p) throw 'Private key out of range'
    return Math.pow(this.g, a) % this.p
  }

  getSharedSecret(priv0, pub1) {
    return Math.pow(pub1, priv0) % this.p
  }
}

function isPrime(num) {
  let out = true
  for (let i = 2; out && i <= Math.sqrt(num); i++) {
    if (!(num % i)) {
      out = false
    }
  }
  return out
}

export default Diffie
