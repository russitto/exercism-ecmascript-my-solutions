const POSITIONS = [
  'first',
  'second',
  'third',
  'fourth',
  'fifth',
  'sixth',
  'seventh',
  'eighth',
  'ninth',
  'tenth',
  'eleventh',
  'twelfth'
]
const FIRST = 'On the __position__ day of Christmas my true love gave to me'
const PHRASES = [
  'twelve Drummers Drumming',
  'eleven Pipers Piping',
  'ten Lords-a-Leaping',
  'nine Ladies Dancing',
  'eight Maids-a-Milking',
  'seven Swans-a-Swimming',
  'six Geese-a-Laying',
  'five Gold Rings',
  'four Calling Birds',
  'three French Hens',
  'two Turtle Doves',
  'and a Partridge in a Pear Tree.\n'
]

class Twelve {
  verse(from, to = 0) {
    if (!to) return this._verse(from)
    let out = []
    for (let i = from; i <= to; i++) {
      out.push(this._verse(i))
    }
    return out.join('\n')
  }

  sing() {
    return this.verse(1, 12)
  }

  _verse(num) {
    if (num < 1) throw 'Number greater than 1'
    let out = []
    for (let i = 0; i < num; i++) {
      let phrase = PHRASES[PHRASES.length - 1 - i]
      // first verse 'and' exception:
      if (num == 1) phrase = phrase.replace(/^and /, '')
      out.unshift(phrase)
    }
    out.unshift(FIRST.replace('__position__', POSITIONS[num-1]))
    return out.join(', ')
  }
}

export default Twelve
