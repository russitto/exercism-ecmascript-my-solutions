const directions = ['north', 'east', 'south', 'west']

class Robot {
  constructor () {
    this.coordinates = [0, 0]
    this.orientIdx = 0
    this.bearing = directions[this.orientIdx]
  }

  orient(direction) {
    this.orientIdx = directions.indexOf(direction)
    if (this.orientIdx < 0) throw 'Invalid Robot Bearing'
    this.bearing = directions[this.orientIdx]
  }

  turnRight() {
    this.orientIdx = (this.orientIdx + 1) % directions.length
    this.bearing = directions[this.orientIdx]
  }

  turnLeft() {
    this.orientIdx--
    if (this.orientIdx < 0) this.orientIdx = directions.length - 1
    this.bearing = directions[this.orientIdx]
  }

  at(x, y) {
    this.coordinates = [x, y]
  }

  advance() {
    switch (this.bearing) {
      case 'north': this.coordinates[1]++; break
      case 'south': this.coordinates[1]--; break
      case 'east':  this.coordinates[0]++; break
      case 'west':  this.coordinates[0]--; break
    }
  }

  evaluate(commands) {
    this._eval(commands).forEach(comm => comm.apply(this))
  }

  instructions(commands) {
    return this._eval(commands).map(comm => comm.name)
  }

  place(conf) {
    this.at(conf.x, conf.y)
    this.orient(conf.direction)
  }

  _eval(commands) {
    return commands.split('').map(letter => {
      switch(letter) {
        case 'A': return this.advance
        case 'R': return this.turnRight
        case 'L': return this.turnLeft
      }
    })
  }
}

export default Robot
