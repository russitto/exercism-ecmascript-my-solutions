import Series from '../series/series'

class LargestSeriesProduct extends Series {
  constructor(number) {
    if (isNaN(number)) {
      throw new Error('Invalid input.')
    }
    super(number)
  }

  largestProduct(num) {
    if (num < 0) {
      throw new Error('Invalid input.')
    }
    if (num == 0) {
      return 1
    }
    const products = this.slices(num).map(slice => {
      let out = 1
      for (let i = 0; i < num; i++) {
        out *= slice[i]
      }
      return out
    })
    return products.reduce((max, value) => {
      if (max >= value) return max
      return value
    }, 0)
  }
}

export default LargestSeriesProduct
