class Trinary {
  constructor(number) {
    const filtered = number.replace(/[^0-2]/g, '')
    this.decimal = 0
    if (number == filtered) {
      let pow = 0
      for (let i = number.length - 1; i >= 0; i--) {
        this.decimal += 3 ** (number.length - 1 - i) * number[i]
      }
    }
  }

  toDecimal() {
    return this.decimal
  }
}

export default Trinary
