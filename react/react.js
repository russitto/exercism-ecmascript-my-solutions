class InputCell {
  constructor(val) {
    this.value = val
  }

  // get value() {
  //   return this.val
  // }

  setValue(val) {
    this.value = val
    document.dispatchEvent(new Event('setValue'))
  }
}

class ComputeCell {
  constructor(inps, fn) {
    this.inputs = inps
    this.callbacks = []
    inps.forEach(inp => {
      document.addEventListener('setValue', () => {
        this.setValue(fn(inps))
      })
    })
    this.setValue(fn(inps))
  }

  setValue(val) {
    const change = val != this.value
    this.value = val
    if (change) {
      this.callbacks.forEach(cb => cb.calculate(this.value))
    }
  }

  addCallback(cb) {
    this.callbacks.push(cb)
  }

  removeCallback(cb) {
    this.callbacks = this.callbacks.filter(c => cb != c)
  }
}

let name = 1
class CallbackCell {
  constructor(fn) {
    this.name = 'callback' + name++
    this.fn = fn
    this.values = []
  }

  calculate(value) {
    this.values.push(value)
  }
}

export {InputCell, ComputeCell, CallbackCell}
