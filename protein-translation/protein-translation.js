export default translate

const TRANS = {
  'AUG': 'Methionine',
  'UUU': 'Phenylalanine',
  'UUC': 'Phenylalanine',
  'UUA': 'Leucine',
  'UUG': 'Leucine',
  'UCU': 'Serine',
  'UCC': 'Serine',
  'UCA': 'Serine',
  'UCG': 'Serine',
  'UAU': 'Tyrosine',
  'UAC': 'Tyrosine',
  'UGU': 'Cysteine',
  'UGC': 'Cysteine',
  'UGG': 'Tryptophan',
  'UAA': 'STOP',
  'UAG': 'STOP',
  'UGA': 'STOP'
}


function translate(str = '') {
  let valid = /^[UCAG]*$/.test(str)
  if (!valid) throw new Error('Invalid codon')
  let codons = str.match(/\w{3}/g)
  if (!codons) return []
  let prots = codons.map(el => TRANS[el])
  let idx = prots.indexOf('STOP')
  if (idx >= 0) prots = prots.splice(0, idx)
  return prots
}
