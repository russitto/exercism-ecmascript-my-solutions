class Anagram {
  constructor(str) {
    str = this.str = str.toLowerCase()
    this.letters = {}
    str = str.split('').sort().join('')
    for (let i = 0; i < str.length; i++) {
      if (!this.letters[str[i]]) {
        this.letters[str[i]] = 0
      }
      this.letters[str[i]]++
    }
  }

  compare(angr) {
    if (this.str == angr.str) {
      return false
    }
    if (this.str.length != angr.str.length) {
      return false
    }
    let out = true
    for (let k in this.letters) {
      if (!angr.letters[k]) {
        out = false
      } else if (this.letters[k] != angr.letters[k]) {
        out = false
      }
    }
    return out
  }

  matches(possibles) {
    if (arguments.length != 1) {
      possibles = Array.from(arguments)
    }
    if (typeof possibles == 'string') {
      possibles = [possibles]
    }
    return possibles.filter(possible => this.compare(new Anagram(possible)))
  }
}

export default Anagram
