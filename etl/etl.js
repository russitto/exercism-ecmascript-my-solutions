export default function transform(old) {
  let out = {}
  for (let k in old) {
    old[k].forEach(letter => {
      out[letter.toLowerCase()] = parseInt(k)
    })
  }
  return out
}
