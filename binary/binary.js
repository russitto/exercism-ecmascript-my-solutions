class Binary {
  constructor(num) {
    const rep = num.replace(/[0,1]/g, '')
    if (rep != '') {
      this.num = 0
    } else {
      this.num = parseInt(num, 2)
    }
  }

  toDecimal() {
    return this.num
  }
}

export default Binary
