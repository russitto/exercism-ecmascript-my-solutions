const sentence = 'For want of a __first__ the __second__ was lost.'
const last = 'And all for the want of a __first__.'

export default function() {
  let out = []
  let args = []
  let qualifier = ''

  if (arguments.length
    && typeof arguments[arguments.length - 1] == 'object'
    && arguments[arguments.length - 1].qualifier) {
    qualifier = arguments[arguments.length - 1].qualifier
  }
  for (let i = 0; i < arguments.length; i++) {
    if (typeof arguments[i] == 'string') {
      args.push(arguments[i])
    }
  }
  for (let i = 0; i < args.length - 1; i++) {
    out.push(sentence.replace('__first__', args[i]).replace('__second__', args[i+1]))
  }
  if (args.length) {
    let repl = args[0]
    if (qualifier) {
      repl = qualifier + ' ' + repl
    }
    out.push(last.replace('__first__', repl))
  }
  return out.join('\n')
}
