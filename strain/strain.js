export default {keep, discard}

function keep(arr, fn, neg = false) {
  let out = []
  arr.forEach(el => {
    let val = fn(el)
    if (neg) val = !val
    if (val) out.push(el)
  })
  return out
}

function discard(arr, fn) {
  return keep(arr, fn, true)
}
