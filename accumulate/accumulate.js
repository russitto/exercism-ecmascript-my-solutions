export default function(elements, fn) {
  let out = []
  elements.forEach(el => out.push(fn(el)))
  return out
}
