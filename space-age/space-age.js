const yearSeconds = 31557600
const mercury = .2408467 * yearSeconds
const venus = .61519726 * yearSeconds
const mars = 1.8808158 * yearSeconds
const jupiter = 11.862615 * yearSeconds
const saturn = 29.447498 * yearSeconds
const uranus = 84.016846 * yearSeconds
const neptune = 164.79132 * yearSeconds

class SpaceAge {
  constructor(seconds) {
    this.seconds = seconds
    this.msecs = seconds * 100
  }

  onEarth() {
    return Math.round(this.msecs / yearSeconds) / 100
  }

  onMercury() {
    return Math.round(this.msecs / mercury) / 100
  }

  onVenus() {
    return Math.round(this.msecs / venus) / 100
  }

  onMars() {
    return Math.round(this.msecs / mars) / 100
  }

  onJupiter() {
    return Math.round(this.msecs / jupiter) / 100
  }

  onSaturn() {
    return Math.round(this.msecs / saturn) / 100
  }

  onUranus() {
    return Math.round(this.msecs / uranus) / 100
  }

  onNeptune() {
    return Math.round(this.msecs / neptune) / 100
  }
}

export default SpaceAge
