export default transpose

function transpose(matr) {
  if (!matr.length) return []
  let maxI = matr.length
  let maxJ = matr.reduce((accum, el) => el.length > accum? el.length: accum, 0)
  matr = matr.map(el => el.split(''))
  let out = new Array(maxJ).fill(undefined).map(el => new Array(maxI))
  for (let i = 0; i < maxI; i++) {
    for (let j = 0; j < maxJ; j++) {
      out[j][i] = matr[i][j] || ' '
    }
  }
  // exception
  if (out[maxJ - 1][maxI - 1] == ' ') {
    let last = out[maxJ - 1].join('')
    last = last.replace(/\s+$/, '')
    out[maxJ - 1] = last.split('')
  }
  return out.map(el => el.join(''))
}
