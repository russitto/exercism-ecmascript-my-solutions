const invalid = '_'

class WordSearch {
  constructor(arr = []) {
    this.values = arr
    this.trans = this.transpile()
  }

  transpile() {
    let matrix = this.values.map(el => el.split(''))
    let out = []
    for (let i = 0; i < matrix.length ; i++) {
      for (let j = 0; j < matrix[i].length ; j++) {
        if (i == 0 && !out[j]) out[j] = []
        out[j][i] = matrix[i][j]
      }
    }
    return out.map(el => el.join(''))
  }

  find(arr) {
    let out = {}
    arr.forEach(el => {
      out[el] = undefined
      let find = this.findOne(el)
      if (find) {
        out[el] = { 
          start: [find.starty+1, find.startx+1],
          end: [find.endy+1, find.endx+1]
        }
      }
    })
    return out
  }

  findOne(str) {
    let out = this.findInX(str)
    if (out) return out
    out = this.findRevInX(str)
    if (out) return out
    out = this.findInY(str)
    if (out) return out
    out = this.findRevInY(str)
    if (out) return out
    out = this.findInYX(str, true, true)
    if (out) return out
    out = this.findInYX(str, true, false)
    if (out) return out
    out = this.findInYX(str, false, false)
    if (out) return out
    out = this.findInYX(str, false, true)
    if (out) return out
    return false
  }

  findInX(str, vals = false) {
    if (!vals) vals = this.values
    let find = false
    for (let i = 0; !find && i < vals.length; i++) {
      let idx = vals[i].indexOf(str)
      if (idx >= 0) {
        find = {startx: idx, starty: i, endx: idx+str.length-1, endy: i}
      }
    }
    return find
  }

  findRevInX(str, vals = false) {
    let rev = str.split('').reverse().join('')
    let find = this.findInX(rev, vals)
    if (!find) return false
    return {startx: find.endx, starty: find.starty, endx: find.startx, endy: find.endy}
  }

  findInY(str) {
    let find = this.findInX(str, this.trans)
    if (!find) return false
    return {startx: find.starty, starty: find.startx, endx: find.endy, endy: find.endx}
  }

  findRevInY(str) {
    let rev = str.split('').reverse().join('')
    let find = this.findInY(rev, this.trans)
    if (!find) return false
    return {startx: find.startx, starty: find.endy, endx: find.endx, endy: find.starty}
  }

  findInYX(str, clockwise = true, reverse = false) {
    if (reverse) {
      str = str.split('').reverse().join('')
    }
    let rotted = rot45(this.values, clockwise)
    str = strToUnds(str)
    let find = this.findInX(str, rotted.data)
    if (!find) return false

    let start = rotted.idx[find.startx +invalid+ find.starty]
    let end = rotted.idx[find.endx +invalid+ find.endy]
    if (reverse) {
      let aux = start
      start = end
      end = aux
    }

    return {
      startx: start[0],
      starty: start[1],
      endx: end[0],
      endy: end[1]
    }
  }
}

export default WordSearch
// let s = new WordSearch([
//   'caca',
//   'cuca',
//   'cala',
//   'caco'
// ])
// s.findInYX('culo', false, false)

function rot45(matr, clockwise = true) {
  // @TODO suppose: square matrix
  let size = matr.length * 2 - 1
  let out = []
  let idx = {}
  for (let i = 0; i < size; i++) {
    out[i] = []
    for (let j = 0; j < size; j++) {
      out[i][j] = invalid
    }
  }

        // 00 10 20 --> 10
        // 01 11 21 --> 00 11
        // 02 12 22 --> 01
  for (let i = 0; i < matr.length; i++) {
    for (let j = 0; j < matr.length; j++) {
      let outI, outJ
      if (clockwise) {
        outJ = matr.length + j - i - 1
        if (outJ > size) {
          outJ = size*2 - outJ
        }
        outI = i + j
      } else {
        outJ = i + j
        if (outJ > size) {
          outJ = size*2 - outJ
        }
        outI = matr.length - 1 - j + i
      }
      out[outI][outJ] = matr[i][j]
      // coords: x_y
      idx[outJ+invalid+outI] = [j, i]
    }
  }
  return {data: out.map(el => el.join('')), idx}
}

function strToUnds(str) {
  return str.split('').join(invalid)
}
