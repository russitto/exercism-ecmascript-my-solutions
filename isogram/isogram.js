class Isogram {
  constructor(values) {
    this._values = values.toLowerCase().replace(/[\s-]/g, '')
  }

  isIsogram() {
    return this._values.length == (new Set(this._values)).size
  }
}

export default Isogram
