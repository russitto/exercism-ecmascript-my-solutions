class List {
  constructor(arr = null) {
    this.values = []
    if (arr) {
      this.values = arr
    }
  }

  append(list) {
    list.values.forEach(el => this.values.push(el))
    return this
  }

  concat(list) {
    let out = new List(this.values)
    return out.append(list)
  }

  filter(condition) {
    let out = []
    this.values.forEach(el => {
      if (condition(el)) {
        out.push(el)
      }
    })
    return new List(out)
  }

  length() {
    return this.values.length
  }

  map(func) {
    let out = []
    this.values.forEach(el => {
      out.push(func(el))
    })
    return new List(out)
  }

  foldl(func, acc) {
    for (let i = 0; i < this.values.length; i++) {
      acc = func(acc, this.values[i])
    }
    return acc
  }

  foldr(func, acc) {
    for (let i = this.values.length - 1; i >= 0; i--) {
      acc = func(acc, this.values[i])
    }
    return acc
  }

  reverse() {
    this.values.reverse()
    return this
  }
}
export default List
