class Element {
  constructor(item, next) {
    if (typeof item == 'undefined') throw new ElementValueRequiredException()
    this.value = item
    if (typeof next != 'undefined') {
      if (!(next instanceof Element)) throw new ElementNextNotInstanceException()
      this.next = next
    }
  }
}

class List {
  constructor() {
  }

  push(el) {
    if (typeof this.head == 'undefined') {
      this.head = el
    } else {
      this.tail.next = el
    }
    this.tail = el
  }

  unshift(el) {
    if (this.head) {
      el.next = this.head
      this.head = el
    } else {
      this.push(el)
    }
  }

  shift() {
    if (this.head) {
      if (this.head.next) {
        this.head = this.head.next
      } else {
        this.head = undefined
      }
    }
  }

  pop() {
    let actual = this.head
    let prev
    while (actual && actual.next) {
      prev = actual
      actual = actual.next
    }
    if (prev) {
      prev.next = undefined
      this.tail = prev
    } else {
      this.head = this.tail = undefined
    }
  }

  static fromArray(arr) {
    let out = new this()
    arr.forEach(item => out.push(new Element(item)))
    return out
  }

  toArray() {
    let actual = this.head
    let out = []
    while (actual && actual.next) {
      out.push(actual.value)
      actual = actual.next
    }
    if (actual) {
      out.push(actual.value)
    }
    return out
  }

  reverse() {
    let other = new List()
    let arr = this.toArray().reverse()
    let tail = this.tail
    arr.forEach(el => this.push(new Element(el)))
    if (arr.length) this.head = tail.next
  }
}

class ElementValueRequiredException extends Error {
}

class ElementNextNotInstanceException extends Error {
}

export { List, Element, ElementValueRequiredException, ElementNextNotInstanceException }
