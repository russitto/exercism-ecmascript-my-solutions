export default solve

const NUMBERS = new Set([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

/*
 ____             _         _____                    ____  _   _  ____ _  ______  _
| __ ) _ __ _   _| |_ ___  |  ___|__  _ __ ___ ___  / ___|| | | |/ ___| |/ / ___|| |
|  _ \| '__| | | | __/ _ \ | |_ / _ \| '__/ __/ _ \ \___ \| | | | |   | ' /\___ \| |
| |_) | |  | |_| | ||  __/ |  _| (_) | | | (_|  __/  ___) | |_| | |___| . \ ___) |_|
|____/|_|   \__,_|\__\___| |_|  \___/|_|  \___\___| |____/ \___/ \____|_|\_\____/(_)
*/

function solve(puzzle) {

  let items = puzzle.split(/==|\+/g).map(el => el.trim().split(''))
  // uniques
  let letters = Array.from(new Set(puzzle.match(/[A-Z]/g)))
  let result = items.pop()

  let solutions = []
  for (let i = 0; i < Math.pow(10, letters.length); i++) {
    let arr = numToArr(i, letters.length)
    if (unique(arr)) {
      solutions.push(arr)
    }
  }

  let found = false
  for (let i = 0; !found && i < solutions.length; i++) {
    let sol = solutions[i]
    let sum = items.reduce((accum, el) => accum + itemToNumber(el, letters, sol), 0)
    if (sum == itemToNumber(result, letters, sol)
      && !someStartWith0([result, ...items], letters, sol)) {
      found = sol
    }
  }
  if (!found) return null
  let out = {}
  for (let i = 0; i < found.length; i++) {
    out[letters[i]] = found[i]
  }
  return out
}

function unique(num) {
  let sett = new Set(num)
  return sett.size == num.length
}

function numToArr(num, length) {
  let out = new Array(length).fill(0)
  let rev = num.toString().split('').reverse()
  for (let i = 0; i < length; i++) {
    if (typeof rev[i] != 'undefined') out[i] = parseInt(rev[i])
  }
  return out.reverse()
}

function itemToNumber(item, letters, possible, arrayForm = false) {
  let out = []
  for (let i = 0; i < item.length; i++) {
    let l = item[i]
    let idx = letters.indexOf(l)
    out.push(possible[idx])
  }
  if (arrayForm) return out
  return parseInt(out.join(''))
}

function someStartWith0(arr, letters, possible) {
  let out = false
  for (let i = 0; !out && i < arr.length; i++) {
    out = itemToNumber(arr[i], letters, possible, true)[0] == 0
  }
  return out
}
