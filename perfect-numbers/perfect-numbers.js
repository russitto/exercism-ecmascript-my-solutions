class PerfectNumber {
  classify(num) {
    const aliq = sum(divisors(num))
    if (aliq < num || num == 1) {
      return 'deficient'
    }
    if (aliq == num) {
      return 'perfect'
    }
    return 'abundant'
  }
}

export default PerfectNumber

function divisors(num) {
  if (num <= 0) {
    throw new Error('Classification is only possible for natural numbers.')
  }

  const divs = [1]
  for (let i = 2; i < num; i++) {
    if (num % i == 0) {
      divs.push(i)
    }
  }
  return divs
}

function sum(nums) {
  return nums.reduce((acc, curr) => acc+ curr)
}
