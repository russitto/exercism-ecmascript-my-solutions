export default {parse}

const NUCL = 'ACGT'
const NUCL_REG = new RegExp('^[' + NUCL + ']*$')
let regexs = []
for (let i = 0; i < NUCL.length; ++i) {
  regexs.push(new RegExp(NUCL[i], 'g'))
}

function parse(str) {
  let valid = NUCL_REG.test(str)
  if (!valid) throw new Error('Invalid nucleotide in strand')
  return regexs.map(r => {
    let m = str.match(r)
    return m? m.length: 0
  }).join(' ')
}
