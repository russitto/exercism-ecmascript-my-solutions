class LinkedList {
  constructor() {
    this.arr = new Array()
    this.nulls = 0
  }

  push(el)    { return this.arr.push(el) }
  pop(el)     { return this.arr.pop(el) } 
  shift(el)   { return this.arr.shift(el) }
  unshift(el) { return this.arr.unshift(el) }

  count() {
    return this.arr.length - this.nulls
  }

  delete(el) {
    const idx = this.arr.indexOf(el)
    if (idx >= 0) {
      this.nulls++
      this.arr[idx] = null
    }
  }
}

export default LinkedList
//let ll = new LinkedList()
//console.log(ll.count())
//ll.push(10)
//console.log(ll.count())
