class TwoBucket {
  constructor(buckOne, buckTwo, goal, starterBuck) {
    this.goal = goal
    this.where = 0
    if (starterBuck == 'two') {
      this.where = 1
    }


    this.bucketSize = [buckOne, buckTwo]
    this.bucketState = [0, 0]
    this._moves = 0
    this.isFill = true
  }

  fill() {
    let other = this.where * -1 + 1
    if (this.bucketState[other] == this.bucketSize[other]) {
      // unfill
      this.bucketState[other] = 0
    } else {
      this.bucketState[this.where] = this.bucketSize[this.where]
    }
    this.isFill = false
    this._moves++
    this.where = other
  }

  change() {
    let where = this.where
    let other = where * -1 + 1
    let inc = this.bucketState[other]
    let avail = this.bucketSize[where] - this.bucketState[where]
    if (inc > avail) {
      inc = avail
    }
    this.bucketState[where] += inc
    this.bucketState[other] -= inc

    this.where = other
    this.isFill = true
    this._moves++
  }

  moves() {
    do {
      // fill or move
      if (this.isFill) {
        this.fill()
      } else {
        this.change()
      }
    } while (this.goal != this.bucketState[0] && this.goal != this.bucketState[1])
    this.goalBucket = this.where? 'two': 'one'
    let other = this.where * -1 + 1
    this.otherBucket = this.bucketState[other]
    return this._moves
  }
}

export default TwoBucket
