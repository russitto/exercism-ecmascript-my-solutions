class PigLatin {
  translate(message) {
    return message.split(' ').map(word => trans(word)).join(' ')
  }
}

function trans(word) {
  const suffix = 'ay'
  if (word.match(/^[aeiou]/)) {
    return word + suffix
  }
  const two = word.substr(0, 2)
  const three = word.substr(0, 3)
  if (['thr', 'sch'].indexOf(three) >= 0) {
    return word.substr(3) + three + suffix
  }
  if (['ch', 'qu', 'th'].indexOf(two) >= 0) {
    return word.substr(2) + two + suffix
  }
  if (word.substr(1, 2) == 'qu') {
    return word.substr(3) + three + suffix
  }
  return word.substr(1) + word.substr(0, 1) + suffix
}

export default PigLatin
