const A_CODE = 'A'.charCodeAt(0)

class Diamond {
  makeDiamond(letter) {
    let length = letter.charCodeAt(0) - A_CODE
    let ret = []
    for (let i = 0; i < length + 1; i++) {
      let l = String.fromCharCode(A_CODE + i)
      let out = repeat(' ', length - i)
      let inn = repeat(' ', i)
      let line = out + l + inn
      ret.push(line + sreverse(line).substr(1))
    }
    let rret = ret.slice().reverse()
    rret.shift()
    return ret.concat(rret).join('\n') + '\n'
  }
}

export default Diamond

function repeat(letter, num) {
  return Array(num+1).join(letter)
}

function sreverse(str) {
  return str.split('').reverse().join('')
}
