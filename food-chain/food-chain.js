const animals = [ 'fly', 'spider', 'bird', 'cat', 'dog', 'goat', 'cow', 'horse' ]
const first = 'I know an old lady who swallowed a __animal__.'
const seconds = [
  '',
  'It wriggled and jiggled and tickled inside her.',
  'How absurd to swallow a bird!',
  'Imagine that, to swallow a cat!',
  'What a hog, to swallow a dog!',
  'Just opened her throat and swallowed a goat!',
  'I don\'t know how she swallowed a cow!',
  ''
]
const intern = 'She swallowed the __animal__ to catch the __animal_1__.'
const internExc = 'She swallowed the bird to catch the spider that wriggled and jiggled and tickled inside her.'
const last = 'I don\'t know why she swallowed the fly. Perhaps she\'ll die.'
const finn = `I know an old lady who swallowed a horse.
She's dead, of course!
`

class Song {
  verse(num) {
    num--
    if (num >= animals.length - 1) {
      return finn
    }
    let out = [first.replace('__animal__', animals[num])]
    if (num > 0 && seconds[num]) {
      out.push(seconds[num])
    }
    for (let i = num; i > 0; i--) {
      let animal = animals[i]
      let animal_1 = animals[i-1]
      let swallow = intern.replace('__animal__', animal).replace('__animal_1__', animal_1)
      if (animal == 'bird') swallow = internExc
      out.push(swallow)
    }
    out.push(last)
    return out.join('\n') + '\n'
  }

  verses(start = 1, end = 8) {
    let out = ''
    for (let i = start; i <= end; i++) {
      out += this.verse(i) + '\n'
    }
    return out
  }
}

export default Song
