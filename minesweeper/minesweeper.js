class Mines {
  annotate(rows) {
    rows = rows.map(el => el.split(''))
    for (let i = 0; i < rows.length; i++) {
      for (let j = 0; j < rows[i].length; j++) {
        if (rows[i][j] != '*') {
          rows[i][j] = sum(rows, i, j)
        }
      }
    }
    return rows.map(el => el.join(''))
  }
}

function sum(rows, i, j) {
  let out = 0
  if (typeof rows[i-1] != 'undefined') {
    out += typeof rows[i-1][j-1] != 'undefined' &&  rows[i-1][j-1] == '*'? 1: 0
    out += typeof rows[i-1][j  ] != 'undefined' &&  rows[i-1][j  ] == '*'? 1: 0
    out += typeof rows[i-1][j+1] != 'undefined' &&  rows[i-1][j+1] == '*'? 1: 0
  }
  out += typeof rows[i][j-1] != 'undefined' &&  rows[i][j-1] == '*'? 1: 0
  out += typeof rows[i][j+1] != 'undefined' &&  rows[i][j+1] == '*'? 1: 0
  if (typeof rows[i+1] != 'undefined') {
    out += typeof rows[i+1][j-1] != 'undefined' &&  rows[i+1][j-1] == '*'? 1: 0
    out += typeof rows[i+1][j  ] != 'undefined' &&  rows[i+1][j  ] == '*'? 1: 0
    out += typeof rows[i+1][j+1] != 'undefined' &&  rows[i+1][j+1] == '*'? 1: 0
  }
  if (out) return out + ''
  return ' '
}

export default Mines
