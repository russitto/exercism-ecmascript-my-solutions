class List {
  constructor(arr = []) {
    this.values = arr
  }

  compare(list) {
    let j1 = JSON.stringify(this.values)
    let j2 = JSON.stringify(list.values)
    j1 = j1.substr(1, j1.length - 2)
    j2 = j2.substr(1, j2.length - 2)

    if (j1 == j2) {
      return 'EQUAL'
    }
    if ((new RegExp(j1)).test(j2)) {
      return 'SUBLIST'
    }
    if ((new RegExp(j2)).test(j1)) {
      return 'SUPERLIST'
    }
    return 'UNEQUAL'
  }
}

export default List
