class Robot {
  constructor() {
    this.nnme = randomName()
  }

  get name() {
    return this.nnme
  }

  reset() {
    this.nnme = randomName()
  }
}

export default Robot

let names = []

function randomName() {
  let rand
  do {
    rand = randomLetter() + randomLetter() + randomNumber() + randomNumber() + randomNumber()
  } while (names.indexOf(rand) >= 0)
  names.push(rand)
  return rand
}

function randomLetter() {
  const base = 'A'.charCodeAt(0)
  const max = 26 // to Z
  const code = base + Math.floor(Math.random() * max)
  return String.fromCharCode(code)
}

function randomNumber() {
  return Math.floor(Math.random() * 10)
}
