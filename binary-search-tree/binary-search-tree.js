class BinarySearchTree {
  constructor(el) {
    this.data = el
  }

  insert(el) {
    if (el <= this.data) {
      if (this.left) {
        this.left.insert(el)
      } else {
        this.left = new BinarySearchTree(el)
      }
    } else {
      if (this.right) {
        this.right.insert(el)
      } else {
        this.right = new BinarySearchTree(el)
      }
    }
  }

  each(fn) {
    if (this.left) this.left.each(fn)
    fn(this.data)
    if (this.right) this.right.each(fn)
  }
}

export default BinarySearchTree
