const NUMBER_STR = `
 _     _  _     _  _  _  _  _ 
| |  | _| _||_||_ |_   ||_||_|
|_|  ||_  _|  | _||_|  ||_| _|
                              `.substr(1)

let numbers = new Array(10).fill(undefined).map((el, i) => separate(i, NUMBER_STR))

class Ocr {
  convert(str) {
    const LINES = str.split('\n')
    if (LINES.length < 5) return convertOneLine(str)
    const LENGTH = parseInt(LINES.length / 4)
    let out = []
    for (let i = 0; i < LENGTH; i++) {
      out.push(convertOneLine(str, i))
    }
    return out.join(',')
  }
}

export default Ocr

function separate(position, str, line = 0) {
  const LENGTH = 3
  const LINES = str.split('\n')
  let out = []
  for (let i = 0; i < 4; i++) {
    out.push(LINES[i + line * 4].substr(position*LENGTH, LENGTH))
  }
  return out.join('\n')
}

function convertOneLine(str, line = 0) {
  const LENGTH = parseInt(str.indexOf('\n') / 3)
  let out = ''
  for (let i = 0; i < LENGTH; i++) {
    let num = separate(i, str, line)
    out += whichNum(num)
  }
  return out
}

function whichNum(str) {
  let out = -1
  for (let i = 0; out == -1 && i < numbers.length; i++) {
    if (str == numbers[i]) out = i
  }
  if (out == -1) return '?'
  return out + '' 
}
