class Isbn {
  constructor(str) {
    this.chars = str.replace(/-/g, '').split('')
  }

  isValid() {
    if (this.chars.length != 10) return false
    if (isNaN(this.chars[9]) && this.chars[9] != 'X') return false

    let out = true
    for (let i = 0; out && i < 9; i++) {
      out = !isNaN(this.chars[i])
    }
    if (!out) return false

    let sum = 0
    for (let i = 0; i < 9; i++) {
      sum += (10 - i) * parseInt(this.chars[i])
    }
    sum += this.chars[9] == 'X'? 10: parseInt(this.chars[9])
    return sum % 11 == 0
  }
}

export default Isbn
