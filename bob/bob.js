const MSG = {
  fine: 'Fine. Be that way!',
  whatever: 'Whatever.',
  chillout: 'Whoa, chill out!',
  sure: 'Sure.'
}

class Bob {
  hey(message) {
    message = message.trim()

    if (message == '') {
      return MSG.fine
    }

    const isAnswer = message.match(/.*\?$/)
    const isCaps = message == message.toUpperCase()
    const isNocaps = message == message.toLowerCase()

    if (isCaps && !isNocaps) {
      return MSG.chillout
    }
    if (isAnswer) {
      return MSG.sure
    }
    return MSG.whatever
  }
}

export default Bob
