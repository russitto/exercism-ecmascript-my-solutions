class Squares {
  constructor(num) {
    this.squareOfSums = ((num * (num + 1)) / 2) ** 2
    this.sumOfSquares = (num * (num + 1) * (num * 2 + 1)) / 6
    this.difference = this.squareOfSums - this.sumOfSquares
  }
}

export default Squares
