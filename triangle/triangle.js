class Triangle {
  constructor(size0, size1, size2) {
    this.size = [size0, size1, size2]
  }

  kind() {
    const size = this.size
    if (size[0] <= 0 || size[1] <= 0 || size[2] <= 0) {
      throw new Error('Some size < 0')
    }
    if (size[0] + size[1] < size[2] || size[0] + size[2] < size[1] || size[1] + size[2] < size[0]) {
      throw new Error('Inequality')
    }

    if (size[0] == size[1] && size[1] == size[2]) {
      return 'equilateral'
    }
    if (size[0] == size[1] || size[1] == size[2] || size[0] == size[2]) {
      return 'isosceles'
    }
    return 'scalene'
  }
}

export default Triangle
