export default { count }

function count(arr) {
  if (!arr.length) return 0
  let out = 0
  arr = arr.map(el => el.split(''))
  let start, end
  let i = 0
  let j = 0
  let count = 0
  let founded = []
  while (i < arr.length && (start = find(arr, i, j))) {
    i = start[0]
    j = start[1]
    founded = [...founded, ...findToRight(arr, i, j)]
    founded = [...founded, ...findToDown(arr, i, j)]
    j++
    if (j >= arr[0].length) {
      i++
      j = 0
    }
  }
  // avoid repetead ones with 'Set'
  founded = Array.from(new Set(founded))
  founded = founded.filter(el => completed(arr, el))
  return founded.length
}

function find(arr, i, j) {
  let out = false
  for (let x = i; !out && x < arr.length; x++) {
    let idx = arr[x].indexOf('+', j)
    if (idx >= 0) {
      out = [x, idx]
    }
  }
  return out
}

function findToRight(arr, i, j) {
  let origI = i
  let origJ = j
  let founded
  let out = []
  while (j < arr[0].length-1 && (founded = find(arr, i+1, j+1))) {
    out.push(origI + '-' + origJ + '-' + founded[0] + '-' + founded[1])
    j = founded[1]
  }
  return out
}

function findToDown(arr, i, j) {
  let origI = i
  let origJ = j
  let founded
  let out = []
  while (i < arr.length - 1 && (founded = find(arr, i+1, j+1))) {
    out.push(origI + '-' + origJ + '-' + founded[0] + '-' + founded[1])
    i = founded[0]
  }
  return out
}

function completed(arr, coords) {
  let sqr = coords.split('-').map(el => parseInt(el))
  let out = true
  for (let j = sqr[1] + 1; out && j < sqr[3]; j++) {
    out = ['-', '+'].indexOf(arr[sqr[0]][j]) >= 0 && ['-', '+'].indexOf(arr[sqr[2]][j]) >= 0
  }
  if (!out) return false
  for (let i = sqr[0] + 1; out && i < sqr[2]; i++) {
    out = ['|', '+'].indexOf(arr[i][sqr[1]]) >= 0 && ['|', '+'].indexOf(arr[i][sqr[3]]) >= 0
  }
  if (!out) return false
  return arr[sqr[0]][sqr[1]] == '+'
    && arr[sqr[2]][sqr[3]] == '+' 
    && arr[sqr[0]][sqr[3]] == '+' 
    && arr[sqr[2]][sqr[1]] == '+' 
}
