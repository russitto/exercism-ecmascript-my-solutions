const students = ['alice', 'bob', 'charlie', 'david', 'eve', 'fred',
  'ginny', 'harriet', 'ileana', 'joseph', 'kincaid', 'larry']

const plants = {
  'C': 'clover',
  'G': 'grass',
  'R': 'radishes',
  'V': 'violets'
}

class Garden {
  constructor(diagram, kids = null) {
    if (kids == null) {
      kids = students
    } else {
      // fix names
      kids = kids.map(k => k.toLowerCase()).sort()
    }

    diagram = diagram.split('\n').map(line => line.split(''))
    if (diagram.length < 2 || diagram[0].length == 0) throw new Error('Invalid diagram.')

    return new Proxy(this, {
      get: (target, name) => {
        let idx = kids.indexOf(name)
        if (idx < 0) throw new Error('Invalid kid name.')
        idx <<= 1
        return [
          plants[diagram[0][idx]],
          plants[diagram[0][idx+1]],
          plants[diagram[1][idx]],
          plants[diagram[1][idx+1]]
        ] 
      }
    })
  }
}

export default Garden
