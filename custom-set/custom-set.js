class CustomSet {
  constructor(arr = []) {
    this.elements = arr
    this.fixArray()
  }

  add(el) {
    if (this.elements.indexOf(el) == -1) {
      this.elements.push(el)
      this.fixArray()
    }
    return this
  }

  fixArray() {
    this.elements = this.elements.filter((el, i) => this.elements.indexOf(el) ==  i).sort()
  }

  empty() {
    return this.elements == 0
  }

  contains(el) {
    return this.elements.indexOf(el) >= 0
  }

  eql(cset) {
    if (this.elements.length != cset.elements.length) return false
    let out = true
    for (let i = 0; out && i < this.elements.length; i++) {
      out = this.elements[i] == cset.elements[i]
    }
    return out
  }

  subset(cset) {
    return this.intersection(cset).eql(this)
  }

  intersection(cset) {
    const inters = this.elements.filter(el => cset.elements.indexOf(el) >= 0)
    return new CustomSet(inters)
  }

  disjoint(cset) {
    return this.intersection(cset).empty()
  }

  difference(cset) {
    const inters = this.elements.filter(el => cset.elements.indexOf(el) == -1)
    return new CustomSet(inters)
  }

  union(cset) {
    this.elements = this.elements.concat(cset.elements)
    this.fixArray()
    return this
  }
}

export default CustomSet
