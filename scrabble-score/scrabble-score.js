const table = {
  D: 2,
  G: 2,
  B: 3,
  C: 3,
  M: 3,
  P: 3,
  F: 4,
  H: 4,
  V: 4,
  W: 4,
  Y: 4,
  K: 5,
  J: 8,
  X: 8,
  Q: 10,
  Z: 10
}

function score(word) {
  let out = 0
  if (word) {
    word = word.toUpperCase()
    for (let i = 0; i < word.length; i++) {
      let val = 1
      if (table[word[i]]) {
        val = table[word[i]]
      }
      out += val
    }
  }
  return out
}

export default score 
