export { encode, decode }

function encode(str) {
  let out = ''
  let count = 0, last = ''
  str.split('').forEach(l => {
    if (l != last && last != '') {
      if (count > 1) {
        out += '' + count
      }
      out += last
      count = 0
    }
    last = l
    count++
  })
  if (count > 1) {
    out += '' + count
  }
  out += last
  return out
}

function decode(compr) {
  let out = ''
  const spl = compr.split(/(\d+|[A-Z ])/).filter(l => l != '')
  for (let i = 0; i < spl.length; i++) {
    if (spl[i] != '') {
      if (!isNaN(parseInt(spl[i]))) {
        let x = repeat(parseInt(spl[i++]), spl[i])
        out += x
      } else {
        out += spl[i]
      }
    }
  }
  return out
}

function repeat(cont, l) {
  return Array(cont+1).join(l)
}
