const connector = 'the'
const first = 'that'
const firstException = 'This'

const nouns = [
  'house that Jack built.',
  'malt',
  'rat',
  'cat',
  'dog',
  'cow with the crumpled horn',
  'maiden all forlorn',
  'man all tattered and torn',
  'priest all shaven and shorn',
  'rooster that crowed in the morn',
  'farmer sowing his corn',
  'horse and the hound and the horn'
]

const verbs = [
  'lay in',
  'ate',
  'killed',
  'worried',
  'tossed',
  'milked',
  'kissed',
  'married',
  'woke',
  'kept',
  'belonged to',
  'is'
]

class House {
  static verse(num) {
    let sentences = []
    for (let i = 0; i < num; i++) {
      let words = []
      let start = first
      let verb = verbs[i]
      if (i == num - 1) {
        start = firstException
        verb = verbs[verbs.length - 1]
      }
      words.push(start)
      words.push(verb)
      words.push(connector)
      words.push(nouns[i])
      sentences.push(words)
    }
    return sentences.map(words => words.join(' ')).reverse()
  }

  static verses(start, end) {
    let out = []
    for (let i = start; i <= end; i++) {
      out = out.concat(this.verse(i))
      if (i < end) out = out.concat('')
    }
    return out
  }
}

export default House
