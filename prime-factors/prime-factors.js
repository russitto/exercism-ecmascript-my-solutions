class PrimeFactors {
  constructor() {
    this.primes = []
  }
  for(num) {
    let out = []
    for (let i = 2; i <= num; i++) {
      if (!(num % i)) {
        if (this.isPrime(i)) {
          do {
            num /= i
            out.push(i)
          } while(!(num %i))
        }
      }
    }
    return out
  }

  isPrime(num) {
    let out = true
    if (this.primes.indexOf(num) >= 0) {
      return true
    }
    for (let i = 2; out && i <= Math.sqrt(num); i++) {
      if (!(num % i)) {
        out = false
      }
    }
    if (out) {
      this.primes.push(num)
    }
    return out
  }
}

export default PrimeFactors
//const p = new PrimeFactors()
//console.log(p.for(93819012551))
