import PrimeFactors from '../prime-factors/prime-factors'

class Prime extends PrimeFactors {
  nth(num) {
    if (num < 1) throw 'Prime is not possible'

    for (let i = 2; this.primes.length < num; i++) {
      this.isPrime(i)
    }
    return this.primes[num - 1]
  }
}

export default Prime
