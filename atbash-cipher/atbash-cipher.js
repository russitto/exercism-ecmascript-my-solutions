const base = 'a'.charCodeAt(0)
const length = 25
const space = 32

export default {
  encode
}

function encode(str) {
  str = str.toLowerCase().replace(/(\s|\.|\,)/g, '')
  let nums = []
  for (let i = 0; i < str.length; i++) {
    if (isNaN(str[i])) {
      nums.push(length - (str.charCodeAt(i) - base) + base)
    } else {
      nums.push(str.charCodeAt(i))
    }
    if (i % 5 == 4) {
      nums.push(space)
    }
  }
  return String.fromCharCode.apply(null, nums).trim()
}
