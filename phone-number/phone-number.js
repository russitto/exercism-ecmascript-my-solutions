class PhoneNumber  {
  constructor(num) {
    this.num = num.replace(/[\(\)\s\.-]/g, '')
    const match = this.num.match(/1?[0-9]{10}/)
    if (!match || match[0] != this.num) {
      this.num = null
    }
    if (this.num && this.num.length == 11) {
      this.num = this.num.substr(1)
    }
  }

  number() {
    return this.num
  }
}

export default PhoneNumber 
