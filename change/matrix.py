def _get_change_making_matrix(c, r):
    m = [[0 for _ in range(r + 1)] for _ in range(c + 1)]
    for i in range(r + 1):
        m[0][i] = i
    return m

print(_get_change_making_matrix(5, 21))
