export default {validate}

function validate(num) {
  if (num < 10) return true
  let parts = num.toString().split('').map(el => parseInt(el))
  let sum = parts.reduce((accum, el) => accum + Math.pow(el, parts.length), 0)
  return sum == num
}
