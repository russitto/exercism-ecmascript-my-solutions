export default { ofSize }

const DIRECTIONS = [
  [0, 1],
  [1, 0],
  [0, -1],
  [-1, 0]
]

function ofSize(size) {
  if (size <= 0) return []
  let matrix = new Array(size).fill(undefined).map(el => new Array(size).fill(0))
  let i = 0
  let j = -1 
  let dir = 0
  for (let x = 1; x <= Math.pow(size, 2); x++) {
    i += DIRECTIONS[dir][0]
    j += DIRECTIONS[dir][1]
    if (typeof matrix[i] == 'undefined'
      || typeof matrix[i][j] == 'undefined'
      || matrix[i][j] != 0) {
      i -= DIRECTIONS[dir][0]
      j -= DIRECTIONS[dir][1]
      dir = (dir + 1) % 4
      i += DIRECTIONS[dir][0]
      j += DIRECTIONS[dir][1]
    }
    matrix[i][j] = x
  }
  return matrix
}
