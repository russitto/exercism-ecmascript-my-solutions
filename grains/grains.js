import BigInteger from './lib/big-integer'

const max = 64

class Grains {
  constructor() {
    this.tot = BigInteger(2).pow(max + 1).minus(1)
  }

  square(num) {
    return BigInteger(2).pow(num-1)
  }

  total() {
    return this.tot
  }
}

export default Grains
