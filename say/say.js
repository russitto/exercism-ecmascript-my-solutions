const first = [
  'zero',
  'one',
  'two',
  'three',
  'four',
  'five',
  'six',
  'seven',
  'eight',
  'nine',
  'ten',
  'eleven',
  'twelve',
  'thirteen'
]

const dec = [
  'twenty',
  'thirty',
  'forty',
  'fifty',
  'sixty'
]

const mult = [
  'thousand',
  'hundred',
  'million',
  'billion'
]

const postfixes = [
  'teen',
  'hundred',
  'thousand',
  'million',
  'billion'
]
const teen = 'teen'
const hundred = 'hundred'

class Say {
  inEnglish(num) {
    if (num < 0 || num > 999999999999) throw new Error('Number must be between 0 and 999,999,999,999.')
    return this.inEnglishArr(num).reverse().join(' ')
  }
  inEnglishArr(num) {
    if (num == 0) return [first[0]]
    let words = []
    let mod10 = num % 10
    let mod100 = num % 100
    let hundreds = div(num % 1000, 100)
    let thousands = div(num % 10000, 1000)

    // console.log(mod100, first.length)
    if (mod100 && mod100 < first.length) {
      words.push(first[mod100])
    } else if (mod100 && mod100 < 20) {
      words.push(first[mod100 % 10] + postfixes[0])
    } else if (mod100 && mod100 < 70) {
      let word = dec[div(mod100, 10) - 2]
      if (mod10) {
        word += '-' + first[mod10]

      }
      words.push(word)
    }

    if (hundreds) {
      words.push(first[hundreds] + ' ' + postfixes[1])
    }
    if (thousands) {
      words.push(first[thousands] + ' ' + postfixes[2])
    }
    /*
    if (num >= 1000 && num < 1000000) {
      console.log('nonmillion', div(num, 1000) % 1000000)
      let nonmillion = this.inEnglishArr(div(num, 1000) % 1000000)
      nonmillion.forEach(w => {
        words.push(w + ' ' + postfixes[2])
      })
    }
    mmm
    */
    let mill = num % 1000000000 
    if (mill >= 1000000) {
      let million = this.inEnglishArr(div(mill, 1000000))
      million.forEach(w => {
        words.push(w + ' ' + postfixes[3])
      })
    }
    if (num >= 1000000000) {
      let billion = this.inEnglishArr(div(num, 1000000000))
      billion.forEach(w => {
        words.push(w + ' ' + postfixes[4])
      })
    }
    return words
  }
}

function div(a, b) {
  return parseInt(a/b)
}

export default Say
