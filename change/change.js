const NO_NEG = 'Negative totals are not allowed.'
const NO_REP = 'The total __num__ cannot be represented in the given currency.'

class Change {
  calculate(arr, value) {
    let optim = optimal(arr, value)
    let i = arr.length
    let j = value
    let out = []
    while (j != 0) {
      if (optim[i][j-arr[i-1]] == optim[i][j]-1) {
        out.unshift(arr[i-1])
        j = j - arr[i-1]
      } else {
        i--
      }
    }
    return out
  }
}

export default Change

function genMatrix(l, value) {
  let m = new Array(l + 1).fill(undefined).map(el => new Array(value + 1).fill(0))
  for (let i = 0; i < m[0].length; i++) {
    m[0][i] = i
  }
  return m
}

function optimal(arr, value) {
  let m = genMatrix(arr.length, value)
  for (let c = 1; c < m.length; c++) {
    for (let r = 1; r < m[0].length; r++) {
      if (arr[c-1] == r) {
        m[c][r] = 1
      } else if (arr[c-1] > r) {
        m[c][r] = m[c-1][r]
      } else {
        m[c][r] = Math.min(m[c - 1][r], 1 + m[c][r - arr[c - 1]])
      }
    }
  }
  return m
}
